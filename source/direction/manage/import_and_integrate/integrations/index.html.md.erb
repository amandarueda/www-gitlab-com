---
layout: markdown_page
title: "Category Direction - Integrations"
description: "This GitLab product category is focused on enabling intuitive collaboration with tools GitLab's customers rely on. Find more information here!"
canonical_path: "/direction/manage/import_and_integrate/integrations/"
---

- TOC
{:toc}

## Integrations

| | |
| --- | --- |
| Stage | [Manage](/direction/manage/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2024-04-22` |

### Introduction and how you can help

Thanks for visiting this category direction page on Integrations in GitLab. This page belongs to the [Import and Integrate](https://about.gitlab.com/handbook/product/categories/#import-and-integrate-group) group of the Manage stage and is maintained by the group's Product Manager, [Magdalena Frankiewicz](https://gitlab.com/m_frankiewicz) ([E-mail](mailto:mfrankiewicz@gitlab.com), [Calendly](https://calendly.com/gitlab-magdalenafrankiewicz/45mins)).

This direction page is a work in progress, and everyone can contribute:
 
- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3AIntegrations&first_page_size=20) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Integrations) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and want to discuss how GitLab can improve Importers, we'd especially love to hear from you.

### Strategy and Themes

<!-- Describe your category. Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

<%= partial("direction/manage/import_and_integrate/integrations/templates/overview") %>

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

<%= partial("direction/manage/import_and_integrate/integrations/templates/next") %>

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

- [Improved set up of GitLab for Slack app for SM instances](https://gitlab.com/gitlab-org/gitlab/-/issues/401920)

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

- [Allow multiple Jira project keys to view Jira issues in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/12609)

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

- [Enable Jira issues at the group level](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#enable-jira-issues-at-the-group-level) *16.9*
- [REST API support for GitLab for Slack app](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#rest-api-support-for-the-gitlab-for-slack-app) *16.9*
- [GitLab for Slack app configurable for groups and instances](https://about.gitlab.com/releases/2024/04/18/gitlab-16-11-released/#gitlab-for-slack-app-configurable-for-groups-and-instances) *16.11*

#### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

<%= partial("direction/manage/import_and_integrate/integrations/templates/not_doing") %>

### Integration Ownership

We are [working to shift responsibilities as DRIs of integrations](https://gitlab.com/gitlab-org/gitlab/-/issues/438682) to individual product teams within the domain areas across GitLab. This will give groups full visibility and ownership in serving customers in their domains, based on group's comprehensive strategy. This will also help to make Integration category more sustainable. We want more integrations to be added over time, and sharing the ownership of these with other groups makes this possible and scalable.

Group Import and Integrate keeps ownership of the foundations of integrations and the [Integration development guidelines](https://docs.gitlab.com/ee/development/integrations/). We keep offering guidance on integrations best practices to anyone that contribute.

As we work to better define ownership of integrations across GitLab, the list below clarifies the current ownership of maintenance, security, and new feature development of each integration.

Security related integrations, owned by group Anti-Abuse:
- [Akismet](https://docs.gitlab.com/ee/integration/akismet.html)
- [Arkose Protect](https://docs.gitlab.com/ee/integration/arkose.html)
- [ReCAPTCHA](https://docs.gitlab.com/ee/integration/recaptcha.html)

CI/CD integrations, owned by group Pipeline Execution:
- [Bamboo CI](https://docs.gitlab.com/ee/user/project/integrations/bamboo.html)
- Drone CI
- [GitHub](https://docs.gitlab.com/ee/user/project/integrations/github.html)
- [Jenkins](https://docs.gitlab.com/ee/integration/jenkins.html)
- JetBrains TeamCity CI
- [Diffblue Cover](https://docs.gitlab.com/ee/integration/diffblue_cover.html)

Integrations owned by group Project Management:
- [Kroki diagrams](https://docs.gitlab.com/ee/administration/integration/kroki.html)
- [Mailgun](https://docs.gitlab.com/ee/administration/integration/mailgun.html)

Integrations owned by group Source Code:
- [PlantUML](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
- [Sourcegraph](https://docs.gitlab.com/ee/integration/sourcegraph.html)
- [Beyond Identity](https://docs.gitlab.com/ee/user/project/integrations/beyond_identity.html)
- [GitGuardian](https://docs.gitlab.com/ee/user/project/integrations/git_guardian.html)

Integrations owned by group Incubation:
- [Apple App Store Connect](https://docs.gitlab.com/ee/user/project/integrations/apple_app_store.html)
- [Google Play](https://docs.gitlab.com/ee/user/project/integrations/google_play.html)

[Elasticsearch integration](https://docs.gitlab.com/ee/integration/elasticsearch.html) is owned by group Global Search.

[Datadog integration](https://docs.gitlab.com/ee/integration/datadog.html) is owned by group Runner.

[Gitpod integration](https://docs.gitlab.com/ee/integration/gitpod.html) is owned by group IDE.

[Harbor integration](https://docs.gitlab.com/ee/user/project/integrations/harbor.html) is owned by group Container Registry.

Packagist is owned by group Package Registry.

[Visual Studio Code extension](https://docs.gitlab.com/ee/user/project/repository/vscode.html) is owned by group Editor Extensions.

External issue trackers, ownership negotiated with group Project Management, currently maintained by group Import and Integrate:
- [Asana](https://docs.gitlab.com/ee/user/project/integrations/asana.html)
- [Bugzilla](https://docs.gitlab.com/ee/user/project/integrations/bugzilla.html)
- [ClickUp](https://docs.gitlab.com/ee/user/project/integrations/clickup.html)
- [Custom issue tracker](https://docs.gitlab.com/ee/user/project/integrations/custom_issue_tracker.html)
- [EWM - IBM Enginnering Workflow Management](https://docs.gitlab.com/ee/user/project/integrations/ewm.html)
- [Jira issue integration](https://docs.gitlab.com/ee/integration/jira/configure.html)
- [GitLab for Jira Cloud app](https://docs.gitlab.com/ee/integration/jira/development_panel.html)
- [Pivotal Tracker](https://docs.gitlab.com/ee/user/project/integrations/pivotal_tracker.html)
- [Redmine](https://docs.gitlab.com/ee/user/project/integrations/redmine.html)
- [YouTrack](https://docs.gitlab.com/ee/user/project/integrations/youtrack.html)

"Notification" integrations, maintained by group Import and Integrate:
- [Slack Notifications](https://docs.gitlab.com/ee/user/project/integrations/slack.html) (deprecated)
- [Slack slash commands](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html)
- [GitLab for Slack app](https://docs.gitlab.com/ee/user/project/integrations/slack.html)
- [Discord](https://docs.gitlab.com/ee/user/project/integrations/discord_notifications.html)
- [Gooogle chat](https://docs.gitlab.com/ee/user/project/integrations/hangouts_chat.html)
- [Irker](https://docs.gitlab.com/ee/user/project/integrations/irker.html)
- [Mattermost notifications](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html)
- [Mattermost slash commands](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html)
- [Microsoft Teams](https://docs.gitlab.com/ee/user/project/integrations/microsoft_teams.html)
- [Pumble](https://docs.gitlab.com/ee/user/project/integrations/pumble.html)
- [Unify Circuit](https://docs.gitlab.com/ee/user/project/integrations/unify_circuit.html)
- [Webex Teams](https://docs.gitlab.com/ee/user/project/integrations/webex_teams.html)
- [Telegram](https://docs.gitlab.com/ee/user/project/integrations/telegram.html)

Other integrations, currently maintained by group Import and Integrate:
- [Trello PowerUp](https://docs.gitlab.com/ee/integration/trello_power_up.html) - ownership negotiated with group Project Management
- [Pipeline status emails](https://docs.gitlab.com/ee/user/project/integrations/pipeline_status_emails.html) - ownership negotiated with stage Verify
- [Emails on push](https://docs.gitlab.com/ee/user/project/integrations/emails_on_push.html)
- [Gmail Actions Buttons](https://docs.gitlab.com/ee/integration/gmail_action_buttons_for_gitlab.html)
- [Mock CI](https://docs.gitlab.com/ee/user/project/integrations/mock_ci.html)
- [Squash TM](https://docs.gitlab.com/ee/user/project/integrations/squash_tm.html) - ownership negotiated with group Product Planning

### Insights

#### Customer Insights

* [Top Growth Opportunities](https://docs.google.com/document/d/1RzZpN-_trbyPZu6-vL4YpDW-3PLj_TaRynJMvt6ZzSc/edit?usp=sharing)
* In our [Integrations Leadership Survey [Dec 2022 Report]](https://docs.google.com/presentation/d/1ovHeGfEnFAWzQTEYJe6SHIC2cUyn4QjO35-uQmVskSo/edit#slide=id.g19bafe62b00_0_588), we investigated the importance of integrations to GitLab users who are either buyers or influence purchase decisions for new tools at their company. We learned that:
    * Integrations has an impact on the decision to purchase GitLab for 62% of respondents.
    * Leaders preferred that GitLab provide deeper integration instead of focusing on new integrations.
    * CI and AppDev are the two most critical workflows when it comes to integration.
    * Leaders were interested in more API support for integrations, as well as the ability to use CLI or SDK to connect home grown systems.
    * Many wanted deeper Slack integration, or use CI integrations to solve for more Slack use cases
    * Native integrations are often not customizable enough to suit varied use cases, requiring too much work to get them functional

### Contributing

If you want to contribute to an existing integrations, you can look for open issues labelled with this integration name, e.g "Integration::Asana" or "Integration::Jira".

If you'd like to contribute a new integration, please first review [Integration development guidelines](https://docs.gitlab.com/ee/development/integrations/). Contact GitLab group owning the domain the new integration fits best, so that the group is aware of the planned contribution.  Import and Integrate offers guidance on integrations best practices during development, by reviewing MRs and answering technical questions.

If you're interested in general Integrations area, [you can find open issues with ~"Category:Integrations" label](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3AIntegrations&first_page_size=20), however this list contains issues specific to particular integrations as well.

Feel free to reach out to the team directly if you need guidance or want
feedback on your work by using the ~"group::import and integrate" label on your open merge requests.

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab/-/blob/master/CONTRIBUTING.md).

### Partnership

If your company is interested in partnering with GitLab, check out the [GitLab Partner Program](https://about.gitlab.com/partners/) page for more info.